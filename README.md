Orbstream Javascript SDK
===========================

```html
<script src="orbstream.js"></script>
<script>
	options = {
		url: "ws://localhost:8080",
		appId: "01980671c6f7e71af9aadb624847005b41915f4c4425e8cf2a11731b3e0b3fd6f39d75a9c88e22b81bceccb1ab8a30ad34188601114e10fb1fa8b94073829ed7"
	}
	Orbstream(options, function(os){
		os.subscribe("channel1", function(data){
			document.write("<span style='color:orange'>incomming data:</span> " + data + "<br />")
		})

		os.online("channel1", function(u){
			document.write("<span style='color:green'>user:</span> " + u + ", connected <br/>")
		})

		os.offline("channel1", function(u){
			document.write("<span style='color:red'>user:</span> " + u + ", disconnected <br />")
		})

		// unsubscribe ?
		// os.unsubscribe("channel1")
	})
</script>
```
